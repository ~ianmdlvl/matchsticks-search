#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

my $main = "./main";
my $verbose = 0;

die "usage: test.pl [-v] [-c COMMAND]\n" unless GetOptions(
    "verbose|v" => \$verbose,
    "command|c=s" => \$main);

&runtest(3,2,"1");
&runtest(4,2,"2");
&runtest(4,3,"1");
&runtest(5,2,"1");
&runtest(5,3,"5/4");
&runtest(5,4,"3/2");
&runtest(6,2,"2");
&runtest(6,3,"3");
&runtest(6,4,"2");
&runtest(6,5,"2");
&runtest(7,2,"1");
&runtest(7,3,"5/4");
&runtest(7,4,"5/3");
&runtest(7,5,"5/3");
print "ok\n";

sub runtest {
    my ($n, $m, $expected) = @_;
    &singletest("$main $n $m", $expected);
    for (my $i = 0; $i < 10; $i++) {
        &singletest("$main -j4 $n $m", $expected);
    }
}

sub singletest {
    my ($cmd, $expected) = @_;
    print "test: $cmd\n" if $verbose;
    open my $pipe, "-|", "$cmd 2>/dev/null"
        or die "open: $!\n";
    my $firstline = <$pipe>;
    chomp($firstline);
    die "$cmd: first line of output not as expected:\n$firstline\n"
        unless $firstline =~ /^(\d+) into (\d+): min fragment ([\d\.e\+\-\/]+)/;
    die "$cmd: min fragment $3, expected $expected\n"
        unless $3 eq $expected;
}
