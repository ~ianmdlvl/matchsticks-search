CFLAGS += -Wall -Wwrite-strings -Wstrict-prototypes -g -O2 -std=gnu99
CFLAGS += $(CMDLINE_CFLAGS)
CPPFLAGS += -DVERSION="\"`git describe --tags --dirty=+`\""
LC_CTYPE=C
LDLIBS = -lpub -lglpk -lm

all: main

index.html: tabulate.py $(wildcard data/*)
	./tabulate.py > $@

test: test.pl main
	./test.pl

clean:
	rm -f main
